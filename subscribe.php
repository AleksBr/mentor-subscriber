<?php
/**
 * Subscribe form and list of subscribers.
 *
 * Plugin Name:         Subscribe
 * Description:         The plugin creates a subscribe form and store subscribers into database.
 * Version:             0.0.1
 * Requires at least:   4.9
 * Requires PHP:        5.5
 * Author:              wpnotpunk
 * License:             MIT
 * Text Domain:         subscribe
 *
 * @package     Subscribe
 */

namespace aleksbr\Subscribe;

defined( 'ABSPATH' ) || exit;

define( 'SUBSCRIBE_VERSION', '0.0.1' );
define( 'SUBSCRIBE_FILE', __FILE__ );
define( 'SUBSCRIBE_PATH', plugin_dir_path( __FILE__ ) );
define( 'SUBSCRIBE_URL', plugin_dir_url( __FILE__ ) );

require_once SUBSCRIBE_PATH . '/vendor/autoload.php';

( new Plugin() )->run();
