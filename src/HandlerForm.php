<?php

namespace aleksbr\Subscribe;

/**
 * Class for processing the form.
 */
class HandlerForm {

	/**
	 * Nonce name.
	 *
	 * @var string
	 */
	private $nonce_name = 'subscribe-action';

	/**
	 * Subscriber data
	 *
	 * @var object
	 */
	private $subscriber;

	/**
	 * Constructor.
	 *
	 * @param object $subscriber Subscriber data.
	 */
	public function __construct( $subscriber ) {
		$this->subscriber = $subscriber;
		add_action( 'wp_ajax_subscribe', [ $this, 'ajax' ] );
		add_action( 'wp_ajax_nopriv_subscribe', [ $this, 'ajax' ] );
	}

	/**
	 * Handler ajax.
	 */
	public function ajax() {
		check_ajax_referer( $this->nonce_name );
		$email = filter_input( INPUT_POST, 'email', FILTER_SANITIZE_EMAIL );

		if ( ! is_email( $email ) ) {
			wp_send_json_error(
				sprintf( // translators: %s - is email address.
					esc_html__( 'The %s is invalid email', 'subscribe' ),
					esc_html( $email )
				)
			);
		}

		if ( 2 === $this->subscriber->add_subscriber( $email ) ) {
			wp_send_json_error(
				sprintf( // translators: %s - is email address.
					esc_html__( 'The %s email is already exists', 'subscribe' ),
					esc_html( $email )
				)
			);
		}

		wp_send_json_success( esc_html__( 'You were successfully subscribed', 'subscribe' ) );
	}
}
