<?php

namespace aleksbr\Subscribe;

/**
 * Class to run the plugin
 */
class Plugin {
	/**
	 * Run plugin scripts.
	 */
	public function run(): void {
		$storage      = new Storage();
		$form         = new Form();
		$handler_form = new HandlerForm( new Subscriber() );
		$storage      = new Storage();
		$storage->hooks();
	}
}

