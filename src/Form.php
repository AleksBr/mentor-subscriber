<?php

namespace aleksbr\Subscribe;

/**
 * Class for form
 */
class Form {

	/**
	 * Form slug
	 *
	 * @var string
	 */
	private $resource_handle = 'subscribe';

	/**
	 * Shortcode slug
	 *
	 * @var string
	 */
	private $shortcode_name = 'subscribe_form';

	/**
	 * Nonce name
	 *
	 * @var string
	 */
	private $nonce_name = 'subscribe-action';

	/**
	 * Nonce variable
	 *
	 * @var string
	 */
	private $nonce;

	/**
	 * Path to styles
	 *
	 * @var string
	 */
	private $styles_path = '/assets/dist/css/main.css';

	/**
	 * Path to scripts
	 *
	 * @var string
	 */
	private $scripts_path = '/assets/dist/js/main.js';

	/**
	 * Constructor
	 */
	public function __construct() {
		add_action( 'init', [ $this, 'create_nonce' ] );
		add_shortcode( $this->shortcode_name, [ $this, 'display_form' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'register_styles' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'register_scripts' ] );
	}

	/**
	 * Create nonce
	 */
	public function create_nonce(): void {
		$this->nonce = wp_create_nonce( $this->nonce_name );
	}

	/**
	 * Register styles
	 */
	public function register_styles(): void {
		wp_register_style(
			$this->resource_handle,
			SUBSCRIBE_URL . $this->styles_path,
			[],
			SUBSCRIBE_VERSION
		);
	}

	/**
	 * Register scripts
	 */
	public function register_scripts(): void {
		wp_register_script(
			$this->resource_handle,
			SUBSCRIBE_URL . $this->scripts_path,
			[],
			SUBSCRIBE_VERSION,
			true
		);
		wp_localize_script(
			$this->resource_handle,
			$this->resource_handle,
			[
				'adminUrl' => admin_url( 'admin-ajax.php' ),
				'nonce'    => $this->nonce,
			]
		);
	}

	/**
	 * Enqueue form in page
	 *
	 * @return false|string
	 */
	public function display_form() {
		wp_enqueue_style( $this->resource_handle );
		wp_enqueue_script( $this->resource_handle );
		ob_start();
		?>
		<form action="" method="POST" class="subscribe-form">
			<div class="subscribe-form-row">
				<input type="email" name="email" class="subscribe-form-field" required>
				<button type="submit" class="subscribe-form-button">
					<?php echo esc_html__( 'Subscribe', 'subscribe' ); ?>
				</button>
			</div>
			<div class="subscribe-form-message" style="display: none"></div>
		</form>
		<?php
		return ob_get_clean();
	}
}
