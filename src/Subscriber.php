<?php

namespace aleksbr\Subscribe;

/**
 * Class for saving the subscriber
 */
class Subscriber {

	/**
	 * Save email to db.
	 *
	 * @param string $email Email address.
	 *
	 * @return bool|int
	 */
	public function add_subscriber( string $email ) {
		global $wpdb;

		// phpcs:ignore WordPress.DB.DirectDatabaseQuery.DirectQuery, WordPress.DB.DirectDatabaseQuery.NoCaching
		return $wpdb->replace(
			$wpdb->prefix . 'subscribers',
			[
				'email' => sanitize_email( $email ),
			],
			[
				'email' => '%s',
			]
		);
	}
}
