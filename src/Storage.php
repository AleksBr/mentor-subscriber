<?php

namespace aleksbr\Subscribe;

/**
 * Class for creating the table
 */
class Storage {

	/**
	 * Run Class hook.
	 */
	public function hooks(): void {
		register_activation_hook( SUBSCRIBE_FILE, [ $this, 'create_table' ] );
	}

	/**
	 * Create database table.
	 */
	public function create_table(): void {
		require_once ABSPATH . 'wp-admin/includes/upgrade.php';
		global $wpdb;

		$table_name = $wpdb->prefix . 'subscribers';

		$sql = "CREATE TABLE {$table_name} (
			email VARCHAR(255) UNIQUE NOT NULL,
			PRIMARY KEY (email)
		) {$wpdb->get_charset_collate()};";

		dbDelta( $sql );
	}
}
