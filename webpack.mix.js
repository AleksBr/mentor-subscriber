// webpack.mix.js

let mix = require('laravel-mix');

mix
    .setPublicPath('assets/dist');

mix
    .sass('assets/src/scss/main.scss', 'css');

mix
    .js('assets/src/js/main.js', 'js');